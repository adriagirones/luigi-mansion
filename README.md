# Controls
Controls per teclat:

| Tecles | Acció |
| ------ | ------ |
|A/W/S/D         |`Moure el personatge`  |
|Tab             |`Obrir/Tancar chat`            |
|Backspace       |`Obrir/Tancar llanterna`            |

# Gameplay
El joc com a mínim necessita de 2 jugadors per començar una partida fins a un màxim de 5 jugadors. Hi ha dos rols: el fantasma i els que cacen el fantasma.

Els caçafantasmes no podran veure el movimient del fantasma, però disposen d'una llanterna la qual poden utilitzar per il·luminar el fantasma i treure-li vida. Un cop aconsegueixin entre tots treure-li la vida al fantasma, el conseguiran caçar. Si tenen la llanterna funcionant, aquesta anirà perdent bateria. Per poder recarregar la bateria hauran d'agafar les bateries (punts grocs) que es van spawnejant pel mapa.

El fantasma haurà d'apropar-se per darrere dels caçafantasmes i col·lisionar amb ells per treure'ls hi una vida.

Condicions de victòria:
- Victòria pel fantasma: Aconsegueix matar a tots els caçafantasmes.
- Victòria pels caçafantasmes: Aconsegueixen sobreviure fins que s'acabi el temps o aconsegueixen caçar el fantasma.

# Crèdits

_Alberto Rivera Barrero_
* **Gitlab**: [ajrivera](https://gitlab.com/ajrivera)

_Daniel García Hernández_
* **Gitlab**: [dabrelad](https://gitlab.com/dabrelad)

_Adrià Gironès Calzada_
* **Gitlab**: [adriagirones](https://gitlab.com/adriagirones)
