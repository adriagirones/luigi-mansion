﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class scr_GameManager : NetworkBehaviour
{
    [SyncVar(hook = nameof(OnTimeChanged))]
    private int time;
    public TMPro.TextMeshProUGUI countText;
    public GameObject ghostWinText;
    public GameObject lantersWinText;

    private int numPlayers;
    private int playersDead;

    public GameObject battery;

    public Transform[] spawnBatteryPoints;

    [ServerCallback]
    private void Start()
    {
        time = 210;
        countText.text = time.ToString();
        StartCoroutine(Countdown());
        StartCoroutine(BatterySpawn());
    }
    public void setNumPlayers(int n)
    {
        if (numPlayers < n)
            numPlayers = n;
        print("HAY PLAYERS " + numPlayers);
    }


    IEnumerator Countdown()
    {
        while(time > 0)
        {
            if(time > 0)
                time--;
            yield return new WaitForSecondsRealtime(1);
        }
        GhostDead();
    }

    IEnumerator BatterySpawn()
    {
        while (true)
        {
            yield return new WaitForSecondsRealtime(30f);
            GameObject nbatt = Instantiate(battery);
            // nbatt.transform.position = new Vector3(0, 0.6f, 0);
            nbatt.transform.position = spawnBatteryPoints[Random.Range(0, spawnBatteryPoints.Length)].position;
            NetworkServer.Spawn(nbatt);
        }
    }

    public void OnTimeChanged(int _old, int _new)
    {
        time = _new;
        int min = time / 60;
        int sec = time % 60;
        if(min < 10 && sec < 10)
            countText.text = "0" + min + ":0" + sec;
        else if (min < 10)
            countText.text = "0" + min + ":" + sec;
        else if (sec < 10)
            countText.text = min + ":0" + sec;
        else
            countText.text = min + ":" + sec;
    }

    [ClientRpc]
    public void GhostDead()
    {
        if (!ghostWinText.activeSelf)
        {
            lantersWinText.SetActive(true);
            StartCoroutine(Disconnect());
        }
       
        //  Time.timeScale = 0;
    }

    [ClientRpc]
    public void playerDead()
    {
        playersDead++;
        if (playersDead == numPlayers && !lantersWinText.activeSelf)
        {
            ghostWinText.SetActive(true);
            //  Time.timeScale = 0;
            StartCoroutine(Disconnect());
        }
    }

    IEnumerator Disconnect()
    {
        yield return new WaitForSecondsRealtime(2f);
        //    if (isLocalPlayer)
            NetworkManager.singleton.StopClient();
    }

}
