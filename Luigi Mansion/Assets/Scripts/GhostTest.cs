﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class GhostTest : MonoBehaviour
{
    private Rigidbody rb;

    public float speed = 120;
    private float rotationMultiplier = 5;
    private float smoothSpeed;


    private void Awake()
    {
        rb = this.GetComponent<Rigidbody>();
    }

    private void Start()
    {
        
            gameObject.layer = 8;
    }

    /* public void startPlayer(int numPlayer)
     {
         this.numPlayer = numPlayer;
     }*/

    private void FixedUpdate()
    {

            rb.velocity = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")) * speed * Time.fixedDeltaTime;
            if (rb.velocity.magnitude > 0)
            {
                smoothSpeed = Mathf.Lerp(smoothSpeed, 0.1f, Time.fixedDeltaTime);
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(rb.velocity), Time.fixedDeltaTime * rotationMultiplier);
            }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Flashlight")
        {
            gameObject.layer = 0;
            print("AAAAAAAAAAAAA");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform.tag == "Flashlight")
        {
            gameObject.layer = 8;
        }
    }
}
