﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class scr_NetworkManagerPlayers : NetworkManager
{
    public Transform spawnGhost;
    public Transform spawnPlayer1;
    public Transform spawnPlayer2;
    public Transform spawnPlayer3;
    public Transform spawnPlayer4;

    public override void OnServerAddPlayer(NetworkConnection conn)
    {
        Transform start = spawnGhost;

        if (numPlayers == 0)
            start = spawnGhost;
        else if(numPlayers == 1)
            start = spawnPlayer1;
        else if (numPlayers == 2)
            start = spawnPlayer2;
        else if (numPlayers == 3)
            start = spawnPlayer3;
        else if (numPlayers == 4)
            start = spawnPlayer4;  

        if (numPlayers == 0)
        {
            GameObject player = Instantiate(spawnPrefabs[0], start.position, start.rotation);
            NetworkServer.AddPlayerForConnection(conn, player);
        }
        else
        {
            GameObject player = Instantiate(playerPrefab, start.position, start.rotation);
            player.GetComponent<scr_Player>().startPlayer(numPlayers);
            NetworkServer.AddPlayerForConnection(conn, player);
        }


    }

}
