﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_test : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        gameObject.layer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(gameObject.layer == 0)
                gameObject.layer = 8;
            else
                gameObject.layer = 0;
        }
    }
}
