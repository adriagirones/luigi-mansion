﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTest : MonoBehaviour
{
    private Rigidbody rb;

    public float speed = 120;
    //public float smoothSpeed = 0.5f;
    private float rotationMultiplier = 5;
    private float smoothSpeed;

    public Material red;
    public Material yellow;
    public Material blue;
    public Material green;
    public Material black;

    private int numPlayer = 3;

    private void Awake()
    {
        rb = this.GetComponent<Rigidbody>();
    }

    private void Start()
    {
        //  if (isLocalPlayer)
        //  {
        print("d");
        if (numPlayer == 0)
            this.GetComponent<MeshRenderer>().material = black;
        else if (numPlayer == 1)
            this.GetComponent<MeshRenderer>().material = red;
        else if (numPlayer == 2)
            this.GetComponent<MeshRenderer>().material = yellow;
        else if (numPlayer == 3)
            this.GetComponent<MeshRenderer>().material = green;
        else if (numPlayer == 4)
            this.GetComponent<MeshRenderer>().material = blue;
        //   }

    }

    public void startPlayer(int numPlayer)
    {
        print("C");
        this.numPlayer = numPlayer;
    }

    private void FixedUpdate()
    {

        rb.velocity = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")) * speed * Time.fixedDeltaTime;
        //print(rb.velocity.magnitude);
        if (rb.velocity.magnitude > 0)
        {
            print("aaaaaaa");
            smoothSpeed = Mathf.Lerp(smoothSpeed, 0.1f, Time.fixedDeltaTime);
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(rb.velocity), Time.fixedDeltaTime * rotationMultiplier);
        }
    }
}
