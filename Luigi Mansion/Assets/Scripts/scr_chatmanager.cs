﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class scr_chatmanager : NetworkBehaviour
{
    
    public TextMeshProUGUI message;
    [HideInInspector]
    public scr_Player player;
    [HideInInspector]
    public scr_Phantom phantom;

    List<string> messageHistory; 

    [SyncVar(hook = nameof(OnMessageWriteChanged))]
    public string messagewrite;

    private void Awake()
    {
        messageHistory = new List<string>();
    }

    void OnMessageWriteChanged(string _Old, string _New)
    {
        print(_New);
        messageHistory.Add(_New);
        message.text = "";

        int a = 0;
        if (messageHistory.Count > 5)
        {
            a = messageHistory.Count - 5;
        }

        for (int i = a; i < messageHistory.Count; i++)
        {
            message.text += messageHistory[i] + "\n";
        }

    }

    public void ButtonMessage(TMP_InputField field)
    {
        if(player != null)
        {
            player.SendMessageWrite(field.text);
        }
        else if (phantom != null)
        {
            phantom.SendMessageWrite(field.text);
        }
    }

    //player script
}
