﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using TMPro;

public class scr_Phantom : NetworkBehaviour
{
    private Rigidbody rb;
    private scr_chatmanager manager;
    private TMP_InputField message;
    private GameObject batteryText;
    private GameObject livesText;

    public float speed = 120;
    private float rotationMultiplier = 5;
    private float smoothSpeed;
    [SyncVar(hook = nameof(OnHPChanged))]
    public int HP = 100;
    private GameObject hpText;
    [SyncVar(hook = nameof(OnBeingLightChanged))]
    private bool isBeingLighted = false;

    private GameObject chat;

    private void Awake()
    {
        rb = this.GetComponent<Rigidbody>();
        hpText = GameObject.Find("GhostLife");
        batteryText = GameObject.Find("BatteryText");
        livesText = GameObject.Find("LivesText");
        manager = GameObject.FindObjectOfType<scr_chatmanager>();
        message = GameObject.FindObjectOfType<TMP_InputField>();
    }

    public override void OnStartLocalPlayer()
    {
        if (manager)
            manager.phantom = this;

        if (isLocalPlayer)
        {
            batteryText?.SetActive(false);
            livesText?.SetActive(false);
        }

        chat = GameObject.Find("ChatGameObject");

        NetworkManager.singleton.gameObject.GetComponent<NetworkManagerHUD>().enabled = false;
    }

    [Command]
    public void SendMessageWrite(string field)
    {
        if (manager)
        {
            manager.messagewrite = "Phantom:" + field;
            message.text = "";
        }
    }

    private void Start()
    {
        if(!isLocalPlayer && !isServer)
            gameObject.layer = 8;

        chat.SetActive(false);
    }

    private void FixedUpdate()
    {
        if (isLocalPlayer)
        {
            if (Input.GetKeyDown(KeyCode.Tab))
                chat.gameObject.SetActive(!chat.gameObject.activeSelf);

            rb.velocity = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")) * speed * Time.fixedDeltaTime;
            if (rb.velocity.magnitude > 0)
            {
                smoothSpeed = Mathf.Lerp(smoothSpeed, 0.1f, Time.fixedDeltaTime);
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(rb.velocity), Time.fixedDeltaTime * rotationMultiplier);
            }
        }
    }

    IEnumerator bajarVida() {
        isBeingLighted = true;
        if(HP > 0)
            HP -= 1;
        print(HP);
        if(HP == 0)
            GameObject.Find("GameManager").GetComponent<scr_GameManager>().GhostDead();
        yield return new WaitForSecondsRealtime(0.1f);
        isBeingLighted = false;
    }

    [ClientRpc]
    void cambiarCapaEnter() {
        if (!isLocalPlayer && !isServer)
            gameObject.layer = 0;
    }

    [ClientRpc]
    void cambiarCapaExit()
    {
        if(!isLocalPlayer && !isServer)
            gameObject.layer = 8;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            print("AAAAAAAAAAA");
            other.GetComponentInParent<scr_Player>().respawn();
        }
    }
    [ServerCallback]
    private void OnTriggerStay(Collider other)
    {
        if (other.transform.tag == "Flashlight")
        {
            print(other.name);
            cambiarCapaEnter();
            if (!isBeingLighted)
                StartCoroutine("bajarVida");
        }
    }

    [ServerCallback]
    private void OnTriggerExit(Collider other)
    {
        if (other.transform.tag == "Flashlight")
            cambiarCapaExit();
    }

    public void OnHPChanged(int oldHP, int newHP)
    {
        HP = newHP;
        hpText.GetComponent<TextMeshProUGUI>().text = "Ghost Life: "+HP.ToString();
    }

    public void OnBeingLightChanged(bool oldLightChanged, bool newLightChanged) {
        isBeingLighted = newLightChanged;
    }

}
