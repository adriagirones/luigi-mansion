﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[AddComponentMenu("")]
public class scr_NetworkRoomPlayerExt : NetworkRoomPlayer
{
    public override void OnStartClient()
    {
        Debug.Log("OnStartClient" + SceneManager.GetActiveScene().path);

        base.OnStartClient();
    }

    [ServerCallback]
    public override void OnClientEnterRoom()
    {
        Debug.Log("OnClientEnterRoom" + SceneManager.GetActiveScene().path);
        this.readyToBegin = true;
    }

    public override void OnClientExitRoom()
    {
        Debug.Log("OnClientExitRoom {0}" + SceneManager.GetActiveScene().path);
    }

    public override void ReadyStateChanged(bool oldReadyState, bool newReadyState)
    {
        Debug.Log("ReadyStateChanged {0}" + newReadyState);
    }

}
