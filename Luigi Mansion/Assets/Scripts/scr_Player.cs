﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using TMPro;

public class scr_Player : NetworkBehaviour
{
    private Rigidbody rb;
    private scr_chatmanager manager;
    private TMP_InputField message;

    public float speed = 120;
    private float rotationMultiplier = 5;
    private float smoothSpeed;
    private Vector3 initialPosition;

    public Material red;
    public Material yellow;
    public Material blue;
    public Material green;
    public Material black;

    [SyncVar]
    private int numPlayer = 6;

    [SyncVar]
    private int battery;
    private GameObject batteryText;
    private GameObject livesText;

    [SyncVar]
    private bool isLightining;

    [SyncVar(hook = nameof(LivesUpdateUI))]
    private int lives;

    private GameObject chat;

    private void Awake()
    {
        rb = this.GetComponent<Rigidbody>();
        manager = GameObject.FindObjectOfType<scr_chatmanager>();
        message = GameObject.FindObjectOfType<TMP_InputField>();
    }
    public override void OnStartLocalPlayer()
    {
        if(manager)
            manager.player = this;
        batteryText = GameObject.Find("BatteryText");
        livesText = GameObject.Find("LivesText");
        isLightining = false;
        initialPosition = transform.position;

        chat = GameObject.Find("ChatGameObject");

        NetworkManager.singleton.gameObject.GetComponent<NetworkManagerHUD>().enabled = false;
    }

    [Command]
    public void SendMessageWrite(string field)
    {
        if (manager)
        {
            manager.messagewrite = "Player" + this.numPlayer + ": " + field;
            message.text = "";
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Battery")
        {
            gotBattery(other.gameObject);
            battery += 30;
            if (battery > 100)
                battery = 100;
            if(isLocalPlayer)
                batteryText.GetComponent<TextMeshProUGUI>().text = "Battery: "+battery.ToString();
        }
    }

    [ServerCallback]
    public void gotBattery(GameObject other)
    {
        GameObject.Destroy(other);
        print("PILLE BATERIA : " + battery);

    }

    private void Start()
    {
        if (numPlayer == 0)
            this.GetComponent<MeshRenderer>().material = black;
        else if (numPlayer == 1)
            this.GetComponent<MeshRenderer>().material = red;
        else if (numPlayer == 2)
            this.GetComponent<MeshRenderer>().material = yellow;
        else if (numPlayer == 3)
            this.GetComponent<MeshRenderer>().material = green;
        else if (numPlayer == 4)
            this.GetComponent<MeshRenderer>().material = blue;

        battery = 100;
        if (isLocalPlayer)
            batteryText.GetComponent<TextMeshProUGUI>().text = "Battery: " + battery.ToString();
        lives = 2;
        if (isLocalPlayer)
            livesText.GetComponent<TextMeshProUGUI>().text = "Lives: " + lives.ToString();

        GameObject.Find("GameManager").GetComponent<scr_GameManager>().setNumPlayers(numPlayer);
        chat.SetActive(false);
    }

    public void startPlayer(int numPlayer)
    {
        this.numPlayer = numPlayer;
        if(manager)
            manager.messagewrite = "Player"+this.numPlayer + " has joined the game";
    }

    private void FixedUpdate()
    {
        if(isLocalPlayer)
        {
            if (Input.GetKeyDown(KeyCode.Backspace) && battery > 0)
            {
                if (isLightining)
                {
                    isLightining = false;
                    StopCoroutine(BatteryConsume());
                    EnableFlashlight();
                    if (isServer)
                        RpcEnableFlashlight();
                    else
                        CmdEnableFlashlight();
                }
                else
                {
                    isLightining = true;
                    StartCoroutine(BatteryConsume());
                    EnableFlashlight();
                    if (isServer)
                        RpcEnableFlashlight();
                    else
                        CmdEnableFlashlight();
                }

            }

            if (Input.GetKeyDown(KeyCode.Tab))
                chat.gameObject.SetActive(!chat.gameObject.activeSelf);

            rb.velocity = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")) * speed * Time.fixedDeltaTime;

            if (rb.velocity.magnitude > 0)
            {
                smoothSpeed = Mathf.Lerp(smoothSpeed, 0.1f, Time.fixedDeltaTime);
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(rb.velocity), Time.fixedDeltaTime * rotationMultiplier);
            }
        }
    }

    IEnumerator BatteryConsume()
    {
        while (isLightining)
        {
            battery--;
            if(isLocalPlayer)
                batteryText.GetComponent<TextMeshProUGUI>().text = "Battery: "+battery.ToString();
            if (battery == 0)
            {
                isLightining = false;
                EnableFlashlight();
                if (isServer)
                    RpcEnableFlashlight();
                else
                    CmdEnableFlashlight();
            }
            yield return new WaitForSecondsRealtime(1f);
        }
    }

    [ClientRpc]
    void RpcEnableFlashlight()
    {
        if (!isLocalPlayer)
            EnableFlashlight();
    }

    [Command]
    void CmdEnableFlashlight()
    {
        EnableFlashlight();
        RpcEnableFlashlight();
    }

    void EnableFlashlight()
    {
        if (transform.GetChild(0).gameObject.activeSelf)
            transform.GetChild(0).transform.position = new Vector3(5000, 5000, 5000);
        else
            transform.GetChild(0).transform.localPosition = new Vector3(0, 0, 0);
        StartCoroutine(hackesito());
    }

    IEnumerator hackesito()
    {
        yield return new WaitForEndOfFrame();
        transform.GetChild(0).gameObject.SetActive(!transform.GetChild(0).gameObject.activeSelf);
    }

    [ServerCallback]
    public void respawn()
    {
        lives--;
        print("NUM LIVES" + lives);
        if (lives == 0)
        {
            GameObject.Find("GameManager").GetComponent<scr_GameManager>().playerDead();
            //  Desconnect();
            StartCoroutine(Disconnect());
        }
        else
        {
            RpcRespawn();
        }
    }
    [ClientRpc]
    void Desconnect()
    {
        if(isLocalPlayer)
            NetworkManager.singleton.StopClient();
    }

    IEnumerator Disconnect()
    {
        yield return new WaitForEndOfFrame();
        Destroy(this.gameObject);
    }

    [ClientRpc]
    void RpcRespawn()
    {
        transform.position = initialPosition;
        StopCoroutine(BatteryConsume());
        battery = 100;

    }

    public void LivesUpdateUI(int _old, int _new)
    {
        lives = _new;
        if (isLocalPlayer)
            livesText.GetComponent<TextMeshProUGUI>().text = "Lives: " + lives.ToString();
    }

    public void onFlashlightEmpty()
    {

    }

}
