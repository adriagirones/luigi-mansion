﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;


[AddComponentMenu("")]

public class scr_NetworkRoomManagerExt : NetworkRoomManager
{
    public override void OnRoomServerSceneChanged(string sceneName)
    {
        print(sceneName.ToString());
        if (sceneName.Equals("Adri")) {
            Time.timeScale = 1;
        }
    }

    public override GameObject OnRoomServerCreateGamePlayer(NetworkConnection conn, GameObject roomPlayer)
    {
        GameObject ghost = (GameObject)Instantiate(spawnPrefabs.Find(prefab => prefab.name.Equals("Ghost")), transform.position, transform.rotation);

        if (roomPlayer.GetComponent<NetworkRoomPlayer>().index == 0)
            return ghost;
        else
            return null;
    }

    /// <summary>
    /// Called just after GamePlayer object is instantiated and just before it replaces RoomPlayer object.
    /// This is the ideal point to pass any data like player name, credentials, tokens, colors, etc.
    /// into the GamePlayer object as it is about to enter the Online scene.
    /// </summary>
    /// <param name="roomPlayer"></param>
    /// <param name="gamePlayer"></param>
    /// <returns>true unless some code in here decides it needs to abort the replacement</returns>
    public override bool OnRoomServerSceneLoadedForPlayer(NetworkConnection conn, GameObject roomPlayer, GameObject gamePlayer)
    {
        gamePlayer.transform.position = startPositions[roomPlayer.GetComponent<NetworkRoomPlayer>().index].position;

        if(roomPlayer.GetComponent<NetworkRoomPlayer>().index > 0)
            gamePlayer.GetComponent<scr_Player>().startPlayer(roomPlayer.GetComponent<NetworkRoomPlayer>().index);

        return true;
    }

    public override void OnRoomStopClient()
    {
        base.OnRoomStopClient();
    }

    public override void OnRoomStopServer()
    {
        base.OnRoomStopServer();
    }

    /*
        This code below is to demonstrate how to do a Start button that only appears for the Host player
        showStartButton is a local bool that's needed because OnRoomServerPlayersReady is only fired when
        all players are ready, but if a player cancels their ready state there's no callback to set it back to false
        Therefore, allPlayersReady is used in combination with showStartButton to show/hide the Start button correctly.
        Setting showStartButton false when the button is pressed hides it in the game scene since NetworkRoomManager
        is set as DontDestroyOnLoad = true.
    */

    bool showStartButton;

    public override void OnRoomServerPlayersReady()
    {
        ServerChangeScene(GameplayScene);
    }

}
